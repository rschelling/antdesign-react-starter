/**
 * AntDesign Theme
 * ---------------
 *
 * Customize the theming of AntDesign by modifying its less variables.
 * All available variable names can be found here: https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
 *
 * Virtual Server Restart required after modifying the variables!
 */

module.exports = {
  // for example: change primary color
  // '@primary-color': '#1da57a',
};
