import React from "react";
import { Button } from "antd";

function App() {
  return (
    <div className="App">
      <h1>Hello World!</h1>
      <Button type="primary">Test</Button>
    </div>
  );
}

export default App;
