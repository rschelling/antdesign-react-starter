# AntDesign-React-Starter

StarterKit for a React App with AntDesign components and styling.
The StarterKit was created according to the "Use in create-react-app" tutorial from AntDesign (https://ant.design/docs/react/use-with-create-react-app) and extended with eslint and prettier.

### Included Packages

- react
- antd - Components, Localization and Theming
- eslint, prettier - Code formatting

### Available Scripts

- `npm start` start a virtual server for development on port 3000
- `npm build` build the app for production

### Theming

To use a customized theme for AntDesign, you can edit AntDesign's less variables in 'src/theme.js'. All available variables can be found here:
https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less

After changing the variables, the virtual server needs to be reloaded.
